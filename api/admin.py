from django.utils.translation import ngettext
from django.contrib import messages
from django.contrib import admin
from django.db import DatabaseError

from api import models
from api.services import matchmaking
from api import exceptions


class ParticipantInline(admin.TabularInline):
    model = models.Participant
    extra = 1


class MatchPlayerInline(admin.TabularInline):
    model = models.MatchPlayer
    extra = 2


class TournamentAdmin(admin.ModelAdmin):
    actions = ["generate_next_round", "update_tournament_ladder"]
    inlines = (ParticipantInline,)

    @admin.action(description="Update selected tournament's ladder")
    def update_tournament_ladder(self, request, queryset):
        if queryset.count() > 1:
            self.message_user(
                request,
                "The action is not allowed for multiple tournaments. Please select only one.",
                messages.ERROR
            )
            return

        try:
            tournament = queryset.first()
            matchmaking.recalculate_ladder(tournament.pk)
            message = f"Successfully updated the ladder for '{tournament.name}'."
            self.message_user(request, message, messages.SUCCESS)

        except exceptions.MatchmakingError as err:
            self.message_user(request, err, messages.ERROR)
        except DatabaseError:
            self.message_user(request, "An unexpected database error occurred", messages.ERROR)

    @admin.action(description="Generate the next round of matches")
    def generate_next_round(self, request, queryset):
        if queryset.count() > 1:
            self.message_user(
                request,
                "The action is not allowed for multiple tournaments. Please select only one.",
                messages.ERROR
            )
            return

        try:
            tournament = queryset.first()
            matches, benched_players = matchmaking.generate_matches(tournament.pk)
            message = (
                f"Successfully generated the next round for '{tournament.name}': "
                f"{len(matches)} matches, {len(benched_players)} benched players."
            )
            self.message_user(request, message, messages.SUCCESS)

        except exceptions.MatchmakingError as err:
            self.message_user(request, err, messages.ERROR)
        except DatabaseError:
            self.message_user(request, "An unexpected database error occurred", messages.ERROR)


class PlayerAdmin(admin.ModelAdmin):
    list_display = ("name", "gender")
    list_filter = ("gender",)
    search_fields = ("name",)
    search_help_text = "Search players by name ..."
    inlines = (ParticipantInline,)


class MatchAdmin(admin.ModelAdmin):
    inlines = (MatchPlayerInline,)
    list_display = ("round", "home_team", "guest_team", "home_points", "guest_points", "is_concluded")
    list_filter = ("round", "is_concluded")

    def home_team(self, match: models.Match) -> str:
        return ", ".join(p.name for p in match.home_players)

    def guest_team(self, match: models.Match) -> str:
        return ", ".join(p.name for p in match.guest_players)


class ParticipantAdmin(admin.ModelAdmin):
    list_display = ("player", "gender", "num_matches", "num_wins", "num_losses", "num_points")
    list_filter = ("tournament", "num_matches", "player__gender")
    search_fields = ("player",)
    search_help_text = "Search players by name ..."
    actions = ["clear_stats"]

    def gender(self, participant: models.Participant) -> str:
        return participant.player.gender

    @admin.action(description="Resets the selected participant's statistics")
    def clear_stats(self, request, queryset):
        updated = queryset.update(num_matches=0, num_points=0)
        self.message_user(request, ngettext(
            "%d participant were successfully reset.",
            "%d participants were successfully reset.",
            updated
        ) % updated, messages.SUCCESS)


admin.site.register(models.Tournament, TournamentAdmin)
admin.site.register(models.Player, PlayerAdmin)
admin.site.register(models.Match, MatchAdmin)
admin.site.register(models.Participant, ParticipantAdmin)
