from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from api import views

urlpatterns = [
    path("tournaments/", views.TournamentsList.as_view()),
    path("tournaments/<int:pk>/", views.TournamentDetails.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
