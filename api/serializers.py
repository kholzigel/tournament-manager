from rest_framework import serializers
from api import models


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Player
        fields = ["id", "name", "gender"]


class TournamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tournament
        fields = ["id", "name", "date", "num_rounds", "matchmaking_strategy", "players"]


class MatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Match
        fields = ["id", "tournament", "round", "is_concluded", "home_points", "guest_points", "players"]
