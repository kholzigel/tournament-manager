class TournamentsApiError(Exception):
    """The base exception for all exceptions in the tournaments app."""


class MatchmakingError(TournamentsApiError):
    """Indicates an error in the matchmaking process."""