import dataclasses
from typing import List, Optional, Protocol, Sequence, Tuple, Dict

from django.db.models import Max
from django.db import transaction

from api import models
from api import exceptions


def draft_next_player(
    players: List[models.Player],
    gender: Optional[models.Player.Gender] = None,
    reverse: bool = False
) -> Optional[models.Player]:
    """Return the next player from the given list of players.

    The function will return the first player (from the top of the list) matching the
    specified gender (if ``gender`` was specified) from the given list of players.
    If ``reverse`` is True, the player will be returned from the bottom of the list
    instead.

    CAUTION: The given ``players`` will be modified by this function.

    Args:
        players: List of players from which to return (Will be modified)
        gender: (Optional) Specifies the gender of the player to be returned
        reverse: (Optional) If True, return the player from the bottom of the list instead

    Return:
        the next player in the list that matches the specified/any gender, or None if the
            list doesn't contain more (such) players
    """
    if not players:
        return None

    start = len(players) - 1 if reverse else 0
    end = -1 if reverse else len(players)
    step = -1 if reverse else 1
    idx = -1

    for i in range(start, end, step):
        if not gender:
            idx = i
            break

        if players[i].gender == gender:
            idx = i
            break

    return None if idx == -1 else players.pop(idx)


@dataclasses.dataclass
class Pairing:
    """Represents a match between two teams of players."""
    home_players: List[models.Player]
    guest_players: List[models.Player]


@dataclasses.dataclass
class TournamentRound:
    """Represents a round of matches played in a tournament."""
    matches: List[Pairing]
    benched_players: List[models.Player]


class MatchmakingStrategy(Protocol):
    """Defines a protocol for matchmaking strategies."""
    participants: Sequence[models.Participant]
    team_size: int

    def generate_pairings(self) -> TournamentRound:
        """Generate the pairings for the next round of matches."""


class PointsBasedMixedMatchmakingStrategy:
    """A matchmaking strategy that takes the players' points and gender into consideration."""
    def __init__(self, participants: List[models.Participant], team_size: int):
        self.participants = sorted(participants, key=lambda p: p.num_points, reverse=True)
        self.team_size = team_size
        self.num_least_played_matches = min([p.num_matches for p in self.participants])
        self.num_most_played_matches = max([p.num_matches for p in self.participants])
        self.num_female_players = sum(
            [1 for p in self.participants if p.player.gender == models.Player.Gender.FEMALE]
        )
        self.num_male_players = sum(
            [1 for p in self.participants if p.player.gender == models.Player.Gender.MALE]
        )
        self._drafted_players = None
        self._benched_players = None

    @property
    def num_mixed_matches(self) -> int:
        """The number of possible mixed matches for the strategy's participants and team size."""
        female_matches = int(self.num_female_players / self.team_size)
        male_matches = int(self.num_male_players / self.team_size)

        return min(male_matches, female_matches)

    def get_matches_by_gender(self, gender: models.Player.Gender) -> int:
        """The max. number of matches in which only players of the specified gender
        participate, after all mixed matches have been figured in."""
        num_players = (
            self.num_male_players
            if gender == models.Player.Gender.MALE
            else self.num_female_players
        )

        num_playing_mixed = self.num_mixed_matches * self.team_size
        remaining_players = num_players - num_playing_mixed

        return int(remaining_players / (self.team_size * 2))

    def _generate_player_pool(self) -> None:
        """Generates the strategy's list of drafted and benched players for the tournament's
         next round of matches.

        In case that there are not enough participants for a full set of matches with the
        specified number of players per match, some players will have to miss a round.
        Players that have completed fewer games will be prioritized for being drafted.
        """
        num_female_matches = self.get_matches_by_gender(models.Player.Gender.FEMALE)
        num_male_matches = self.get_matches_by_gender(models.Player.Gender.MALE)
        num_mixed_matches = self.num_mixed_matches
        num_females_to_bench = (
            self.num_female_players
            - (num_mixed_matches * self.team_size)
            - (num_female_matches * 2 * self.team_size)
        )
        num_males_to_bench = (
            self.num_male_players
            - (num_mixed_matches * self.team_size)
            - (num_male_matches * 2 * self.team_size)
        )
        num_players_to_bench = num_males_to_bench + num_females_to_bench
        self._drafted_players = []
        self._benched_players = []

        if num_players_to_bench > 0:
            # There are not enough players, we need to triage
            num_females_benched = 0
            num_males_benched = 0

            for participant in self.participants:
                player_gender = participant.player.gender

                if participant.num_matches < self.num_most_played_matches:
                    # Participant was benched last round, add them no matter what
                    self._drafted_players.append(participant.player)

                elif (num_females_benched + num_males_benched) < num_players_to_bench:
                    # We still need to bench some players
                    if player_gender == models.Player.Gender.MALE:
                        if num_males_benched >= num_males_to_bench:
                            self._drafted_players.append(participant.player)
                        else:
                            self._benched_players.append(participant.player)
                            num_males_benched += 1
                    else:
                        if num_females_benched >= num_females_to_bench:
                            self._drafted_players.append(participant.player)
                        else:
                            self._benched_players.append(participant.player)
                            num_females_benched += 1

                else:
                    # Already benched enough players, everyone else is good to go
                    self._drafted_players.append(participant.player)

        else:
            # Got exactly the right amount of players, everyone will play
            self._drafted_players = [p.player for p in self.participants]

    def _get_next_team(self, mix_gender: bool = False) -> List[models.Player]:
        """Get the next team from the strategies list of drafted players.

        The players for the team are drafted in alternating order from the top and bottom
        of the strategy's list. Drafting will begin with the first element of the list.

        Args:
            mix_gender: (Optional) If True, the method will draft players of alternating
                gender for the team

        Returns:
            the team as list of players
        """
        team = []
        next_gender = None

        for i in range(1, self.team_size + 1):
            reverse = i % 2 == 0
            player = draft_next_player(self._drafted_players, next_gender, reverse)

            if mix_gender:
                next_gender = (
                    models.Player.Gender.MALE
                    if player.gender == models.Player.Gender.FEMALE
                    else models.Player.Gender.FEMALE
                )

            team.append(player)

        return team

    def generate_pairings(self) -> TournamentRound:
        """Generate the next round of matches for the strategy's tournament.

        Returns:
            a TournamentRound object with the next rounds' match-pairings and benched players.

        Raises:
            exception.MatchmakingError: if there are not enough participants to generate
                a single match
        """
        if len(self.participants) < (self.team_size * 2):
            raise exceptions.MatchmakingError(
                f"The tournament must have at least {self.team_size * 2} participants."
            )

        self._generate_player_pool()
        matches = []

        # First do pairings for mixed matches
        for match_num in range(0, self.num_mixed_matches):
            matches.append(
                Pairing(
                    home_players=self._get_next_team(True),
                    guest_players=self._get_next_team(True)
                )
            )

        # Pair remaining players, gender doesn't matter anymore
        for match_num in range(0, int(len(self._drafted_players) / (self.team_size * 2))):
            matches.append(
                Pairing(
                    home_players=self._get_next_team(),
                    guest_players=self._get_next_team()
                )
            )

        return TournamentRound(matches=matches, benched_players=self._benched_players)


class UniquePairingMatchmakingStrategy:
    """A matchmaking strategy that tries to generate unique pairings for teams and matches."""


def _recalculate_ladder(tournament_id: int) -> None:
    """Recalculate the points and rounds played of each tournament participant."""
    match_players = (
        models.MatchPlayer.objects.filter(
            match__tournament_id=tournament_id
        ).order_by("player").prefetch_related("match")
    )

    player_stats = {}
    for match_player in match_players:
        stats = player_stats.setdefault(
            match_player.player.id,
            {
                "num_points": 0,
                "num_matches": 0,
                "num_wins": 0,
                "num_losses": 0
            }
        )
        stats["num_matches"] += 1

        if match_player.team == models.MatchPlayer.TeamTypes.HOME:
            stats["num_points"] += match_player.match.home_points

            if match_player.match.home_points > match_player.match.guest_points:
                stats["num_wins"] += 1
            elif match_player.match.home_points < match_player.match.guest_points:
                stats["num_losses"] += 1

        else:
            stats["num_points"] += match_player.match.guest_points

            if match_player.match.guest_points > match_player.match.home_points:
                stats["num_wins"] += 1
            elif match_player.match.guest_points < match_player.match.home_points:
                stats["num_losses"] += 1

    participants = (
        models.Participant.objects.filter(
            tournament_id=tournament_id
        ).order_by("player")
    )

    with transaction.atomic():
        for participant in participants:
            stats = player_stats.get(participant.player.id, None)

            if not stats:
                continue

            participant.num_matches = stats["num_matches"]
            participant.num_points = stats["num_points"]
            participant.num_losses = stats["num_losses"]
            participant.num_wins = stats["num_wins"]
            participant.save()


def recalculate_ladder(tournament_id: int) -> None:
    """(Re-)calculate the ladder for the specified tournament.

    Raises:
        model.Tournament.DoesNotExist: if the specified tournament doesn't exist
        exception.MatchmakingError: if the tournament's current round is not yet
            concluded
    """
    tournament = models.Tournament.objects.get(pk=tournament_id)

    result = tournament.matches.aggregate(Max("round"))
    max_round_num = result["round__max"]

    if tournament.matches.filter(round=max_round_num, is_concluded=False):
        raise exceptions.MatchmakingError(
            "The tournament's current round is not yet concluded"
        )

    _recalculate_ladder(tournament_id)


def generate_matches(tournament_id: int) -> Tuple[List[models.Match], List[models.Player]]:
    """Generate the next round of matches for the specified tournament.

    Args:
        tournament_id: The ID of the tournament for which the next round of matches
            should be generated

    Returns:
        a tuple with the list of generated matches and list of benched players

    Raises:
        model.Tournament.DoesNotExist: if the specified tournament doesn't exist
        exceptions.MatchmakingError: if an error occurred generating the next round
    """
    tournament = models.Tournament.objects.get(pk=tournament_id)

    result = tournament.matches.aggregate(Max("round"))
    max_round_num = result["round__max"]

    if max_round_num == tournament.num_rounds:
        raise exceptions.MatchmakingError(
            "The tournament already has its maximum number of rounds"
        )

    if tournament.matches.filter(round=max_round_num, is_concluded=False):
        raise exceptions.MatchmakingError(
            "The tournament's current round is not yet concluded"
        )

    result = models.Participant.objects.filter(tournament_id=tournament_id).aggregate(
        Max("num_matches")
    )
    max_matches_num = result["num_matches__max"]

    if max_round_num and max_round_num > max_matches_num:
        _recalculate_ladder(tournament_id)

    participants = models.Participant.objects.filter(
        tournament_id=tournament_id).order_by("-num_points")

    strategy = PointsBasedMixedMatchmakingStrategy(participants, 2)
    next_round = strategy.generate_pairings()
    new_round_num = max_round_num + 1 if max_round_num is not None else 1
    matches = []

    with transaction.atomic():
        for pairing in next_round.matches:
            match = models.Match(tournament=tournament, round=new_round_num)
            match.save()

            for player in pairing.home_players:
                match_player = models.MatchPlayer(
                    match=match,
                    player=player,
                    team=models.MatchPlayer.TeamTypes.HOME
                )
                match_player.save()

            for player in pairing.guest_players:
                match_player = models.MatchPlayer(
                    match=match,
                    player=player,
                    team=models.MatchPlayer.TeamTypes.GUEST
                )
                match_player.save()

            matches.append(match)

    return matches, next_round.benched_players


def generate_pairings_report(tournament_id: int) -> Dict:
    """Generate a report of all unique pairings of players for the specified tournament.

    The returned dictionary maps every player in the tournament to two lists of pairing
    tuples; one for teammates, and one for opponents:
        player_id -> {
            "teammates": [(num_paired, player, player), ...],
            "opponents": [(num_paired, player, player), ...]
        }

        Where the elements in the tuple

        - 0: The number these players were paired
        - 1: The teammate/opponent

    Both lists are sorted by the number of parings in descending order.

    Args:
        tournament_id: The ID of the tournament for which to generate the report

    Returns:
        a dictionary that maps each player to their teammate and opponent pairings

    Raises:
        model.Tournament.DoesNotExist: if the specified tournament doesn't exist
    """
    tournament = models.Tournament.objects.get(pk=tournament_id)
    pairing_stats = {}

    def add_match(match_player: models.MatchPlayer) -> None:
        """Adds the given match's pairings to the statistics dictionary."""
        player_stats = pairing_stats.setdefault(
            match_player.player.pk,
            {
                "player": match_player.player,
                "teammates": {},
                "opponents": {}
            }
        )

        other_players = models.MatchPlayer.objects.filter(
            match=match_player.match,
            match__tournament=tournament,
        ).exclude(player=match_player.player).prefetch_related("player")

        for other_player in other_players:
            if other_player.team == match_player.team:
                num_pairings = player_stats["teammates"].setdefault(other_player.player.pk, 0)
                player_stats["teammates"][other_player.player.pk] = num_pairings + 1

            else:
                num_pairings = player_stats["opponents"].setdefault(other_player.player.pk, 0)
                player_stats["opponents"][other_player.player.pk] = num_pairings + 1

    for player in tournament.players.all():
        player_matches = models.MatchPlayer.objects.filter(
            match__tournament=tournament,
            player=player
        ).prefetch_related("match").prefetch_related("player")

        for player_match in player_matches:
            add_match(player_match)

    report = {}
    for player_id, pairings in pairing_stats.items():
        report_stats = report.setdefault(player_id, {"teammates": [], "opponents": []})
        current_player = pairings["player"]
        report_stats["player"] = current_player

        for teammate_id, num_pairings in pairings["teammates"].items():
            teammate = pairing_stats[teammate_id]["player"]
            report_stats["teammates"].append((num_pairings, teammate))

        for opponent_id, num_pairings in pairings["opponents"].items():
            opponent = pairing_stats[opponent_id]["player"]
            report_stats["opponents"].append((num_pairings, opponent))

    for _, player_stats in report.items():
        player_stats["teammates"].sort(key=lambda r: r[0], reverse=True)
        player_stats["opponents"].sort(key=lambda r: r[0], reverse=True)

    return report
