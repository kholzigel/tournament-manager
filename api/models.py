from typing import List
from django.db import models


class Player(models.Model):
    """Represents a player in the Tournaments app."""

    class Gender(models.TextChoices):
        """Represents the biological sex of a Player."""
        MALE = "male", "Male"
        FEMALE = "female", "Female"

    name = models.CharField(max_length=64)
    gender = models.CharField(max_length=6, choices=Gender.choices, default=Gender.MALE)

    def __str__(self):
        return self.name


class Tournament(models.Model):
    """Represents a tournament in the Tournaments app."""

    class MatchmakingStrategies(models.IntegerChoices):
        """Represents the different strategies used to determine match pairings."""
        RANDOM = 1, "Randomly"
        POINTS = 2, "By player points"

    name = models.CharField(max_length=64)
    date = models.DateTimeField()
    num_rounds = models.IntegerField("Number of rounds played", default=4)
    matchmaking_strategy = models.IntegerField(
        choices=MatchmakingStrategies.choices, default=MatchmakingStrategies.POINTS
    )
    players = models.ManyToManyField(Player, through="Participant", related_name="tournaments")

    def __str__(self):
        return self.name


class Match(models.Model):
    """Represents a match played in a tournament."""
    tournament = models.ForeignKey(Tournament, related_name="matches", on_delete=models.CASCADE)
    round = models.IntegerField(default=1)
    is_concluded = models.BooleanField(default=False)
    home_points = models.IntegerField(default=0)
    guest_points = models.IntegerField(default=0)
    players = models.ManyToManyField(Player, through="MatchPlayer", related_name="matches")

    @property
    def home_players(self) -> List[Player]:
        return [player for player in self.players.filter(
            matchplayer__team=MatchPlayer.TeamTypes.HOME)
        ]

    @property
    def guest_players(self) -> List[Player]:
        return [player for player in self.players.filter(
            matchplayer__team=MatchPlayer.TeamTypes.GUEST)
        ]

    def __str__(self):
        home_players = [player.name for player in self.home_players]
        guest_players = [player.name for player in self.guest_players]

        return f"Round {self.round}: {', '.join(home_players)} VS {', '.join(guest_players)}"

    class Meta:
        verbose_name_plural = "Matches"


class Participant(models.Model):
    """Represents a player participating in a tournament."""
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    tournament = models.ForeignKey(Tournament, on_delete=models.CASCADE)
    num_points = models.IntegerField(default=0)
    num_matches = models.IntegerField(default=0)
    num_wins = models.IntegerField(default=0)
    num_losses = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.player.name} (matches: {self.num_matches}, points: {self.num_points})"


class MatchPlayer(models.Model):
    """Represents a player participating in a match."""
    class TeamTypes(models.TextChoices):
        HOME = "home", "Home Team"
        GUEST = "guest", "Guest Team"

    match = models.ForeignKey(Match, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.CharField(max_length=5, choices=TeamTypes.choices, default=TeamTypes.HOME)

    def __str__(self):
        return f"{self.player.name} ({self.team})"


