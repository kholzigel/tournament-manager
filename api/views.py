from rest_framework import generics
from api import models
from api import serializers


class TournamentsList(generics.ListCreateAPIView):
    """Represents a collection of tournaments."""
    queryset = models.Tournament.objects.all()
    serializer_class = serializers.TournamentSerializer


class TournamentDetails(generics.RetrieveUpdateDestroyAPIView):
    """Represents the details view of a tournament object."""
    queryset = models.Tournament.objects.all()
    serializer_class = serializers.TournamentSerializer
