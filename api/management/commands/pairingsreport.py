import argparse

from django.core.management import base

from api import models
from api.services import matchmaking


class Command(base.BaseCommand):
    help = "Generates the next round of pairings for the specified tournament."

    def add_arguments(self, parser: argparse.ArgumentParser):
        parser.add_argument("tournament_id", type=int)

    def handle(self, *args, **options):
        tournament_id = options["tournament_id"]

        try:
            report = matchmaking.generate_pairings_report(tournament_id)

        except models.Tournament.DoesNotExist:
            raise base.CommandError(f"Tournament with ID {tournament_id} does not exist.")

        self.stdout.write(
            self.style.SUCCESS(f"Generating the pairings report ...")
        )

        for player, stats in report.items():
            self.stdout.write(f"\n{stats['player'].name}")
            self.stdout.write(f"  Teammates:")

            for num, teammate in stats["teammates"]:
                self.stdout.write(f"    {num:2d} {teammate}")

            self.stdout.write(f"\n  Opponents:")
            for num, opponent in stats["opponents"]:
                self.stdout.write(f"    {num:2d}x {opponent}")

        self.stdout.write(self.style.SUCCESS(f"\nFinished generating report."))