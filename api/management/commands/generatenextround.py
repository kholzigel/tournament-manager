import argparse
import enum

from django.core.management import base
import django.db

from api import exceptions
from api import models
from api.services import matchmaking


class MatchmakingStrategies(enum.Enum):
    """Defines the different matchmaking strategies used in the app."""
    POINTS = "points"
    RANDOM = "random"


class Command(base.BaseCommand):
    help = "Generates the next round of pairings for the specified tournament."

    def add_arguments(self, parser: argparse.ArgumentParser):
        parser.add_argument("tournament_id", type=int)
        parser.add_argument("-s", "--strategy", default=MatchmakingStrategies.POINTS.value,
                            choices=[e.value for e in MatchmakingStrategies])

    def handle(self, *args, **options):
        tournament_id = options["tournament_id"]
        strategy = MatchmakingStrategies(options["strategy"])

        try:
            matches, benched_players = matchmaking.generate_matches(tournament_id)
        except models.Tournament.DoesNotExist:
            raise base.CommandError(f"Tournament with ID {tournament_id} does not exist.")
        except exceptions.MatchmakingError as err:
            raise base.CommandError(f"Failed to generate the next tournament round: {err}")
        except django.db.DatabaseError as err:
            raise base.CommandError(f"Failed to persist generated matches to database: {err}")

        self.stdout.write(self.style.SUCCESS(
            f"Successfully generated the next tournament round ({matches[0].round}): "
            f"{len(matches)} matches, {len(benched_players)} benched players."
        ))
